import time, random, datetime, json, names, logging, sys, uuid
from threading import Thread
from kafka import KafkaProducer
from kafka.errors import KafkaError
from kafka.admin import KafkaAdminClient, NewTopic
from dynaconf import settings


producer = KafkaProducer(retries=5, bootstrap_servers=settings.APPCONFIG.KAFKASERVERS)


log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
formatter = logging.Formatter(fmt="%(asctime)s %(levelname)s: %(message)s", 
                          datefmt="%Y-%m-%d - %H:%M:%S")
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)

log.addHandler(ch)

log.debug("running with config: %s", settings.as_dict())

def create_topic():
    admin_client = KafkaAdminClient(bootstrap_servers=settings.APPCONFIG.KAFKASERVERS, client_id='test')
    topic_metadata = admin_client.list_topics()
    
    if settings.APPCONFIG.TOPIC not in topic_metadata:
        log.info("Topics werden erstellt")
        topic_list = []
        topic_list.append(NewTopic(name=settings.APPCONFIG.TOPIC, num_partitions=10, replication_factor=1))
        admin_client.create_topics(new_topics=topic_list, validate_only=False)

def send_order(a):
    count = 0
    #mytimer=random.uniform(0.1, 1)
    #mytimer=random.randint(1, 10)
    mytimer=1
    while True:
        count += 1
        time.sleep(mytimer)
        now = datetime.datetime.now()
        mytime = now.strftime('%Y-%m-%d %H:%M:%S')
        logging.info('%s - PID: %s sleeps %s run %s', mytime, a, mytimer, count)
        order = {
            "firstname": names.get_first_name(),
            "lastname": names.get_last_name(),
            "orderid": str(uuid.uuid4()),
            "sent": mytime,
            "pid": a,
            "sleeps": mytimer,
            "count": count
            }
        log.info("Schreibe: %s", json.dumps(order).encode('utf-8'))

        producer.send(settings.APPCONFIG.TOPIC, json.dumps(order).encode('utf-8'))
        producer.flush()

create_topic()

for x in range(settings.APPCONFIG.RPS):
    t = Thread(target=send_order, args=(x,), daemon=True)
    t.start()
    
while True:
    time.sleep(1)

#answer = input('Do you want to exit?\n')