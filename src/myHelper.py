import time
from dynaconf import settings
import myLogger


def pause():
    count = 1
    if settings.APPCONFIG.FAILRATE == 0:
            # Treten in der Simulation keine Fehler (settings.APPCONFIG.FAILRATE = 0) auf, 
            # wird diese Bedingung durchlaufen.
            myLogger.info('Failrate: %s. Läuft!'%settings.APPCONFIG.FAILRATE)
            myLogger.info('normale Verarbeitung: %s Sleep: %s'%(count, settings.APPCONFIG.SUCCESSDELAY))
            time.sleep(settings.APPCONFIG.SUCCESSDELAY)
            count += 1
    else:
        # Treten keine Fehler auf wied die Verarbeitung zerzögert. 
        # Beispiele: 
        # - Eine externe Schnittstelle eines Partners könnte langsam antworten.
        # - weitere Klassen benötigen länger um Preise zu liefern.
        if count <= settings.APPCONFIG.FAILRATE:
            myLogger.info('verzögerte Verarbeitung: %s Sleep: %s'%(count, settings.APPCONFIG.FAILDELAY))
            #print(f'Message failed mit Counter: {count} Sleep: {settings.APPCONFIG.FAILDELAY}')
            time.sleep(settings.APPCONFIG.FAILDELAY)
            count += 1
        else:
            # Im Normalbetrieb treten keine Fehler auf. Die Verarbeitung läuft nahezu Verzögerungungsfrei.
            #print(f'Message erfolgreich mit Counter: {count} Sleep: {settings.APPCONFIG.SUCCESSDELAY}')
            myLogger.info('normale Verarbeitung: %s Sleep: %s'%(count, settings.APPCONFIG.SUCCESSDELAY))
            time.sleep(settings.APPCONFIG.SUCCESSDELAY)
            count += 1
    
pause()     