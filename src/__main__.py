import time, datetime, json, names, uuid

from threading import Thread
from kafka import KafkaProducer
from kafka.errors import KafkaError
from kafka.admin import KafkaAdminClient, NewTopic
from dynaconf import settings

import myLogger

producer = KafkaProducer(retries=5, bootstrap_servers=settings.APPCONFIG.KAFKASERVERS)

myLogger.debug(str("Config variablen: %s"%settings.as_dict()))

def create_topic():
    admin_client = KafkaAdminClient(bootstrap_servers=settings.APPCONFIG.KAFKASERVERS, client_id='test')
    topic_metadata = admin_client.list_topics()
    
    if settings.APPCONFIG.TOPIC not in topic_metadata:
        myLogger.info("Topics werden erstellt")
        topic_list = []
        topic_list.append(NewTopic(name=settings.APPCONFIG.TOPIC, num_partitions=10, replication_factor=1))
        admin_client.create_topics(new_topics=topic_list, validate_only=False)

def send_order(a):
    count = 0
    mytimer=1
    while True:
        count += 1
        time.sleep(mytimer)
        now = datetime.datetime.now()
        mytime = now.strftime('%Y-%m-%d %H:%M:%S')
        myLogger.info('%s - PID: %s sleeps %s run %s'%(mytime, a, mytimer, count))
        order = {
            "firstname": names.get_first_name(),
            "lastname": names.get_last_name(),
            "orderid": str(uuid.uuid4()),
            "sent": mytime,
            "pid": a,
            "sleeps": mytimer,
            "count": count
            }
        myLogger.info("Schreibe: %s"%json.dumps(order).encode('utf-8'))

        producer.send(settings.APPCONFIG.TOPIC, json.dumps(order).encode('utf-8'))
        producer.flush()

create_topic()

for x in range(settings.APPCONFIG.RPS):
    t = Thread(target=send_order, args=(x,), daemon=True)
    t.start()
    
while True:
    time.sleep(1)
